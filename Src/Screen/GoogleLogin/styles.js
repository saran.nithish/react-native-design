import { StyleSheet } from 'react-native';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp, } from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';
import globalColor from '../../globlaStyle/color'
import { color } from 'react-native-reanimated';

export default StyleSheet.create({
    img: {
        width: '100%',
        height: '100%'
    },
    face: {
       // top:hp('10%'),
        width: '100%',
        height: hp('10%'),
    },

    face2: {
        top:hp('2%'),
        width: '100%',
        height: hp('10%'),
    },
    Or:{
        fontSize:hp('2.2%'),
        fontFamily:fontProperties.bold,
    }
});
