import React, { useState, useEffect } from 'react';
import { StatusBar } from 'react-native';
import { View, Text, Image, ImageBackground, Alert } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globlaStyle/styles';
import styles from './styles';
/////library
import Font from 'react-native-vector-icons/FontAwesome5'
import Ions from 'react-native-vector-icons/Ionicons'
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DropShadow from "react-native-drop-shadow";
///components
import { imagepath } from '../../globlaStyle/imagePaths';
import globalColor from '../../globlaStyle/color';
import { fontProperties } from '../../globlaStyle/font';
import { TouchableOpacity } from 'react-native';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';

const GoogleLogin = (props) => {
    const [userInfo, setUserInfo] = useState(null);
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [error, setError] = useState(null);

    const [gettingLoginStatus, setGettingLoginStatus] = useState(true);

    useEffect(() => {
        GoogleSignin.configure();
    }, []);
    //   const WEBCLINDID = '57690608532-vu0lgrc08m2td8nko1bp1tph821jk8gb.apps.googleusercontent.com'


    const _isSignedIn = async () => {
        const isSignedIn = await GoogleSignin.isSignedIn();
        if (isSignedIn) {
            alert('User is already signed in');
            // Set User Info if user is already signed in
            _getCurrentUserInfo();
        } else {
            console.log('Please Login');
        }
        setGettingLoginStatus(false);
    };

    const _getCurrentUserInfo = async () => {
        try {
            let info = await GoogleSignin.signInSilently();
            console.log('User Info --> ', info);
            setUserInfo(info);
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_REQUIRED) {
                alert('User has not signed in yet');
                console.log('User has not signed in yet');
            } else {
                alert("Unable to get user's info");
                console.log("Unable to get user's info");
            }
        }
    };
    const _signIn = async () => {
        let Data;
        try {
          await GoogleSignin.hasPlayServices();
          let userInfo = await GoogleSignin.signIn();
          Data = userInfo;
        } catch (error) {
          if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            console.log('ERROR1', error);
            // user cancelled the login flow
          } else if (error.code === statusCodes.IN_PROGRESS) {
            console.log('ERROR2', error);
            // operation (e.g. sign in) is in progress already
          } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            console.log('ERROR3', error);
            // play services not available or outdated
          } else {
            console.log('ERROR4', error);
            // some other error happened
          }
        }
        console.log(Data)
        return Data;
        // Get the users ID token
        // const { idToken } = await GoogleSignin.signIn();

        // // Create a Google credential with the token
        // const googleCredential = auth.GoogleAuthProvider.credential(idToken);

        // // Sign-in the user with the credential
        // return auth().signInWithCredential(googleCredential);

    }
        ;

    const _signOut = async () => {
        setGettingLoginStatus(true);
        // Remove user session from the device.
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            // Removing user Info
            setUserInfo(null);
        } catch (error) {
            console.error(error);
        }
        setGettingLoginStatus(false);
    };


    return (

        <View style={[globlaStyle.global, { backgroundColor: '#fff' }]}>
            <StatusBar />
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

                <TouchableOpacity style={styles.face}>
                    <Image
                        source={imagepath.FbLogin}
                        style={styles.img}
                    />
                </TouchableOpacity>
                <View style={{ paddingTop: hp('2%') }}>
                    <Text style={styles.Or}>OR</Text>
                </View>
                {/* <TouchableOpacity style={styles.face2}>
                    <Image
                        source={imagepath.google}
                        style={styles.img}
                    />
                </TouchableOpacity> */}
                {userInfo !== null ? (
                    <>

                        <Text style={styles.text}>
                            Name: {userInfo.user.name}
                        </Text>
                        <Text style={styles.text}>
                            Email: {userInfo.user.email}
                        </Text>
                        <TouchableOpacity
                            style={styles.buttonStyle}
                            onPress={_signOut}>
                            <Text>Logout</Text>
                        </TouchableOpacity>
                    </>
                ) : (
                    <GoogleSigninButton
                        style={{ width: '80%', height: hp('8%') }}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Dark}
                        onPress={_signIn}
                    // disabled={this.state.isSigninInProgress}
                    />)}

            </View>
        </View>
    )
};



export default GoogleLogin;