import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
////////library
import Font from 'react-native-vector-icons/FontAwesome'
import StarRating from 'react-native-star-rating';
import Icon from 'react-native-vector-icons/Ionicons';
import Ant from 'react-native-vector-icons/AntDesign';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
///styles
import styles from './styles';
import globlaStyle from '../../globlaStyle/styles';
import globalColor from '../../globlaStyle/color';


const TableDetails = () => {

    const [Star, setStar] = React.useState()

    const onStarRatingPress = (rating) => {
        setStar(rating)
    }

    return (

        <View style={styles.maincont}>
            <StatusBar translucent backgroundColor="transparent" />
            <ScrollView>
                <View style={styles.curveShape}>
                    <Image
                        source={require('../../../assets/image/chair3.jpg')}
                        style={styles.img}
                    />
                </View>
                <View style={styles.contentbox}>
                    <View style={styles.contentview}>
                        <View style={globlaStyle.flex}>
                            <Text style={globlaStyle.subtitle}>
                                Table Set
                </Text>
                        </View>
                        <View>
                            <Font
                                name="heart"
                                size={hp('3%')}
                                color={globalColor.heartcolor}
                                style={styles.hearticonstyle}
                            />
                        </View>
                    </View>
                    <View style={globlaStyle.row}>
                        <View style={styles.pricesview}>
                            <View style={styles.pirceview}>
                                <Text style={styles.pirceText}>Rp 1.149.000</Text>
                            </View>
                        </View>
                        <View style={styles.priceView2}>
                            <Text style={styles.pirceText2}>Rp 1.149.000</Text>
                        </View>
                    </View>
                    <View style={styles.starView}>
                        <StarRating
                            disabled={false}
                            starSize={hp('2.5%')}
                            maxStars={5}
                            rating={Star}
                            selectedStar={(rating) => onStarRatingPress(rating)}
                            fullStarColor={globalColor.starcolor}
                            emptyStarColor={globalColor.starcolor}
                        />
                        <View style={styles.rateTextview}>
                            <Text>4.5</Text>
                        </View>
                    </View>
                    <View style={styles.colorview}>
                        <Text style={globlaStyle.subtitle}>
                            {'Color:'}
                        </Text>
                        <View style={styles.circleView}>
                            <View style={styles.circleShap} />
                        </View>
                        <View style={styles.circleView}>
                            <View style={styles.circleShap3} />
                        </View>
                        <View style={styles.circleView}>
                            <View style={styles.circleShap4} />
                        </View>
                    </View>
                    <View>
                        <Text style={globlaStyle.subtitle}>Descriptions</Text>
                    </View>
                    <View style={styles.desView}>
                        <Text style={globlaStyle.dectext}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                    </Text>
                    </View>
                    <View style={globlaStyle.row}>
                        <View style={styles.titleline} />
                        <Icon
                            name="chevron-down-outline"
                            size={hp('3%')}
                            color={globalColor.downarrowcolor}
                        />
                    </View>
                </View>
            </ScrollView>
            <View style={styles.upperline}>
                <View style={styles.chatViews}>
                    <View style={styles.chaticonView}>
                        <View style={globlaStyle.row}>
                            <Ant
                                name="message1"
                                size={hp('4%')}
                                color={globalColor.chatcolor}
                                style={styles.messageiconStyle}
                            />
                            <View style={styles.chatView}>
                                <Text style={styles.chattext}>Chat</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.checkBtnView}>
                        <TouchableOpacity>
                            <View style={styles.checkBtn}>
                                <Text style={styles.checkbtntext}>
                                    Add to Cart
                    </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
};

export default TableDetails;