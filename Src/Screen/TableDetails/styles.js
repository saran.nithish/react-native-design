import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';
import globalColor from '../../globlaStyle/color'

export default StyleSheet.create({
    maincont: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
    },
    subCont: {
        flex: 1,
        padding: hp('1.5%')
    },
    inputMain: {
        height: hp('10%'),
        width: '100%',
        paddingHorizontal: hp('1.5%')
    },
    inputSub: {
        flex: 1,
        paddingHorizontal: hp('2%'),
        bottom: hp('4%')
    },
    inputStyle: {
        height: hp('7%'),
        borderWidth: hp('.15%'),
        borderColor: '#CCCDD1',
        backgroundColor: '#fff',
        borderRadius: hp('3.4%'),
        paddingHorizontal: hp('2%'),
        justifyContent: 'center',
    },
    circleShap: {
        height: hp('2%'),
        width: hp('2%'),
        borderRadius: hp('1.5%'),
        backgroundColor: "#9D4508",
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: hp('.20%')
    },
    circleShap3: {
        height: hp('2%'),
        width: hp('2%'),
        borderRadius: hp('1.5%'),
        backgroundColor: "#686868",
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleShap4: {
        height: hp('2%'),
        width: hp('2%'),
        borderRadius: hp('1.5%'),
        backgroundColor: "#BACF39",
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleline: {
        marginTop: hp('1.2%'),
        width: wp('80%'),
        height: hp('.15%'),
        backgroundColor: '#D7D7D7'
    },

    checkBtn: {
        backgroundColor: globalColor.primaryColor,
        padding: hp('1%'),
        borderRadius: hp('2%'),
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    curveShape: {
        height: hp('45%'),
        transform: [{ scaleX: 1.5 }],
        borderBottomStartRadius: 200,
        borderBottomEndRadius: 200,
        overflow: 'hidden',
    },
    img: {
        width: '100%',
        height: '100%',
    },
    contentview: {
        paddingVertical: hp('1%'),
        flexDirection: 'row'
    },
    contentbox: {
        flex: 1.5,
        padding: hp('3%')
    },
    pirceText: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('2%'),
        textAlign: 'center',
        color: globalColor.black
    },
    pirceText2: {
        fontFamily: fontProperties.regular,
        fontSize: hp('2%'),
        color: globalColor.black
    },
    pirceview: {
        backgroundColor: globalColor.primaryColor,
        padding: hp('.5%'),
        
    },
    priceView2: {
        marginLeft: hp('4%'),
        justifyContent: 'center'
    },
    starView: {
        flexDirection: 'row',
        paddingVertical: hp('2%')
    },
    rateTextview: {
        marginLeft: hp('2%')
    },
    colorText: {
        fontSize: hp('2%'),
        fontWeight: 'bold'
    },
    circleView: {
        marginLeft: hp('1%'),
        justifyContent: 'center'
    },
    desView: {
        paddingVertical: hp('1%')
    },
    addtext: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('2.5%'),
        color: globalColor.black
    },
    addTextView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    chattext: {
        fontSize: hp('2%'),
        color: '#757575',
        fontFamily: fontProperties.regular
    },
    chatView: {
        marginLeft: hp('4%'),
        marginTop: hp('.4%')
    },
    hearticonstyle: {
        marginTop: hp('.3%')
    },
    messageiconStyle: {
        left: hp('3%'),
    },
    upperline: {
        borderTopWidth: hp('.15%'),
        borderTopColor: '#CFCFCF',
        margin: hp('2%')
    },
    chatViews: {
        flexDirection: 'row',
        paddingVertical: hp('1%')
    },
    chaticonView: {
        flex: 1,
        justifyContent: 'center'
    },
    pricesview: {
        flex: 0.8,
    },
    colorview: {
        flexDirection: 'row',
        paddingVertical: hp('2%'),
    },
    checkbtntext: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('2.5%'),
        textAlign: 'center'
    },
});
