import React from 'react';
import { StatusBar } from 'react-native';
import { View, Text, Image, ImageBackground, TouchableOpacity } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globlaStyle/styles';
import styles from './styles';
/////library
import Font from 'react-native-vector-icons/FontAwesome5'
import Ions from 'react-native-vector-icons/Ionicons'
import Aut from 'react-native-vector-icons/AntDesign'
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DropShadow from "react-native-drop-shadow";
///components
import PopularPlace from '../../Components/PopularPlace';
import { imagepath } from '../../globlaStyle/imagePaths';
import LinearGradient from 'react-native-linear-gradient';

const PlaceDetails = (props) => {
    const [skills, setskills] = React.useState([
        {
            id: '1',
            title: 'Job title',
            image: imagepath.home1,
            icon: imagepath.travel1

        },
        {
            id: '2',
            title: 'Request Date: ',
            image: imagepath.home2,
            icon: imagepath.travel2

        },

        {
            id: '3',
            title: 'Name',
            image: imagepath.home3,
            icon: imagepath.travel3

        },

        {
            id: '4',
            title: 'Name',
            image: imagepath.home3,
            icon: imagepath.travel4
        },
    ])

    return (

        <View style={[globlaStyle.global]}>
            <StatusBar translucent backgroundColor="transparent" />
            <LinearGradient colors={['#F4E9E3', '#F4F6F6',]} style={styles.linearGradient}>
                <View style={styles.row}>
                    <TouchableOpacity onPress={() => { props.navigation.navigate('Japan') }}>
                        <Ions
                            name="chevron-back-outline"
                            size={hp('4%')}
                            color={'#333'}

                        />
                    </TouchableOpacity>
                    <View style={{ justifyContent: 'center', marginLeft: hp('2%') }}>
                        <Text style={styles.headingTitle}>
                            Fuji-san <Text style={{ color: '#989395' }}>(Japan)</Text>
                        </Text>
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.imagestyle}>
                        <Image source={imagepath.home1}
                            style={styles.img} />
                    </View>
                    <View style={styles.row}>
                        <View style={globlaStyle.flex}>
                            <View>
                                <Text style={styles.subtitle}>Depart</Text>
                            </View>
                            <View style={styles.top}>
                                <View style={styles.box}>
                                    <View style={styles.row}>
                                        <Font
                                            name="calendar-day"
                                            size={hp('2.2%')}
                                            color={'#838387'}
                                        />
                                        <View style={styles.dateView}>
                                            <Text style={styles.dateText}>20 Aug</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.flex0}>
                            <View>
                                <Text style={styles.subtitle}>Return</Text>
                            </View>
                            <View style={styles.top}>
                                <View style={styles.box}>
                                    <View style={styles.row}>
                                    <Font
                                            name="calendar-day"
                                            size={hp('2.2%')}
                                            color={'#838387'}
                                        />
                                        <View style={styles.dateView}>
                                            <Text style={styles.dateText}>27 Aug</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>


                    <View style={styles.row1}>
                        <View style={globlaStyle.flex}>
                            <View>
                                <Text style={styles.subtitle}>Person</Text>
                            </View>
                            <View style={styles.top}>
                                <View style={styles.box}>
                                    <View style={styles.row3}>
                                        <View style={{ marginRight: hp('5%') }}>
                                            <Aut
                                                name="minus"
                                                size={hp('3%')}
                                                color={'#838387'}

                                            />
                                        </View>
                                        <View style={styles.dateView}>
                                            <Text style={styles.dateText}>2</Text>
                                        </View>
                                        <View style={{ marginLeft: hp('5%') }}>
                                            <Aut
                                                name="plus"
                                                size={hp('3%')}
                                                color={'#0AB7C2'}

                                            />
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.flex0}>
                            <View>
                                <Text style={styles.subtitle}>Transport</Text>
                            </View>
                            <View style={styles.top}>
                                <View style={styles.box}>
                                    <View style={styles.row}>
                                        <Font
                                            name="plane"
                                            size={hp('2.5%')}
                                            color={'#838387'}

                                        />
                                        <View style={styles.dateView}>
                                            <Text style={styles.dateText}>Flight</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.row2}>
                        <View style={globlaStyle.flex}>
                            <View style={styles.row1}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Text style={styles.price}>Total:$254</Text>
                                </View>
                            </View>
                        </View>
                        <View>
                            <TouchableOpacity>
                                <View style={styles.checkBtn}>
                                    <Text style={styles.checkbtntext}>
                                        Checkout
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </LinearGradient>
        </View>
    )
};



export default PlaceDetails;