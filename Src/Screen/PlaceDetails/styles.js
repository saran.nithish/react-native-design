import { StyleSheet } from 'react-native';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';
import globalColor from '../../globlaStyle/color'
import { color } from 'react-native-reanimated';

export default StyleSheet.create({
    linearGradient:{
        flex:1,
        paddingHorizontal:hp('2%')
    },
    row:{
        flexDirection:'row',
        paddingVertical:hp('5%')
    },
    row1:{
        flexDirection:'row',
        bottom:'4%'
        //paddingVertical:hp('2%')
    },
    row3:{
        flexDirection:'row',
       // bottom:'4%'
        //paddingVertical:hp('2%')
    },
    row2:{
        flexDirection:'row',
        paddingVertical:hp('2%')
    },
    headingTitle:{
        fontSize:hp('3%'),
        fontFamily:fontProperties.semiBold
    },
    imagestyle:{
        paddingHorizontal:hp('5%'),
        width:'100%',
        height: hp('48%'),
    },
    img:{
        width:'100%',
        height:'100%'
    },
    box:{
        height: hp('6%'), 
        backgroundColor: '#EDEDED',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:hp('1.5%')
    },
    top:{
        paddingTop:hp('2%')
    },
    dateView:{
        justifyContent: 'center', marginLeft: hp('2%')
    },
    dateText:{
        fontFamily:fontProperties.bold,
        fontSize:hp('2%')
    },
    flex0:{
        flex:1,
        left:hp('1%')
    },
    checkBtn: {
        backgroundColor:'#00B7C2',
        padding: hp('1.5%'),
        width: hp('20%'),
        borderRadius: hp('2%'),
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    checkbtntext: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('2.5%'),
        textAlign: 'center',
        color:'#fff'
    },
    price:{
        paddingTop:hp('2%'),
        fontSize:hp('3%'),
        fontFamily:fontProperties.bold,
    },
    subtitle:{
        fontSize:hp('2.2%'),
        fontFamily:fontProperties.regular,
        color:'#A7A8AB'
    }
});
