import { StyleSheet } from 'react-native';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';
import globalColor from '../../globlaStyle/color'
import { color } from 'react-native-reanimated';

export default StyleSheet.create({
    linearGradient:{
        flex:1,
        paddingHorizontal:hp('2%')
    },
    nameView:{
        flexDirection:'row',
        paddingTop:hp('10%')
    },
    profileimageview:{
        alignItems:'flex-end',
       // right:hp('2%'),
        paddingTop:hp('2%')
    },
    inputStyle: {
        height: hp('7%'),
        // borderWidth: hp('.15%'),
        // borderColor: '#CCCDD1',
        backgroundColor: '#E7E7E7',
        //borderRadius: hp('3.4%'),
        paddingHorizontal: hp('2%'),
        justifyContent: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    row:{
        flexDirection:'row',
    },
    titleHead:{
        fontSize: hp('5%'),
        fontFamily:fontProperties.bold,
        left:4
    },
    searchicon: {
        flex: 0.1,
        top: hp('1.5%')
    },
    searchinput: {
        flex: 1,
       // top: hp('.5%')
    },
    productview:{
        marginLeft:hp('1%'),
        paddingTop:hp('1%')
       // bottom:hp('8%'),
    },
    choose:{
        fontSize:hp('2.2%'),
        fontFamily:fontProperties.regular,
        color: '#C2C2C2'
    },
});
