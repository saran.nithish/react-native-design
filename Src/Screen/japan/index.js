import React from 'react';
import { StatusBar } from 'react-native';
import { View, Text, Image, ImageBackground } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globlaStyle/styles';
import styles from './styles';
/////library
import Font from 'react-native-vector-icons/FontAwesome5'
import Ions from 'react-native-vector-icons/Ionicons'
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DropShadow from "react-native-drop-shadow";
///components
import PopularPlace from '../../Components/PopularPlace';
import { imagepath } from '../../globlaStyle/imagePaths';
import globalColor from '../../globlaStyle/color';
import { TouchableOpacity } from 'react-native';
import LastPlace from '../../Components/LastPlace';
import Travel from '../../Components/Travel';
import Popular from '../../Components/popular';
import LinearGradient from 'react-native-linear-gradient';
import { fontProperties } from '../../globlaStyle/font';
import { SnapshotViewIOS } from 'react-native';

const Japan = (props) => {
    const [skills, setskills] = React.useState([
        {
            id: '1',
            title: 'Travel',
            name:'Fuji-san',
            country:'Japan',
            image: imagepath.home1,
            icon: imagepath.travel1

        },
        {
            id: '2',
            title: 'Height',
            name:'SunSet',
            country:'Thailand',
            image: imagepath.home2,
            icon: imagepath.travel2

        },

        {
            id: '3',
            title: 'Hotol',
            name:'Fuji-san',
            country:'Japan',
            image: imagepath.home3,
            icon: imagepath.travel3

        },

        {
            id: '4',
            title: 'Bus rental',
            name:'Fuji-san',
            country:'Japan',
            image: imagepath.home3,
            icon: imagepath.travel4
        },
    ])

    return (

        <View style={[globlaStyle.global]}>
            <StatusBar translucent backgroundColor="transparent" />
            <LinearGradient colors={['#F4E9E3', '#F4F6F6',]} style={styles.linearGradient}>
                <View style={styles.nameView}>
                    <View style={[globlaStyle.flex2, { justifyContent: 'center' }]}>
                        <View style={styles.row}>
                            <View>
                                <Image source={imagepath.hand}
                                    style={globlaStyle.profileimage} />
                            </View>
                            <View style={{justifyContent:'center'}}>
                                <Text style={styles.titleHead}>Hi! Tran</Text>
                            </View>
                        </View>
                        <Text style={styles.choose}>Where Would you like to go?</Text>
                    </View>
                    <View style={{ flex: 0.5 }}>
                        <View style={styles.profileimageview}>
                            <Image source={imagepath.profile}
                                style={globlaStyle.profileimage} />
                        </View>
                    </View>
                </View>
                <View style={{ paddingVertical: hp('2%') }}>
                    <View style={styles.inputStyle}>
                        <View style={globlaStyle.row}>
                            <View style={styles.searchinput}>
                                <TextInput
                                    placeholder='Search'
                                    style={styles.textinput}
                                />
                            </View>
                            <View style={styles.searchicon}>
                                <Font
                                    name="search"
                                    size={hp('3%')}
                                    color={'#89898E'}
                                />
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ paddingVertical: hp('2%') }}>
                    <Travel data={skills} navigation={props.navigation} />
                </View>
                <View style={[styles.row]}>
                    <View style={globlaStyle.flex}>
                        <Text style={{
                            fontFamily:fontProperties.bold,
                            fontSize: hp('2.5%')
                        }}>
                            Popular
                        </Text>
                    </View>
                    <View style={[globlaStyle.flex, { paddingTop: hp('1%'), alignItems: 'flex-end' }]}>
                        <View style={{ flexDirection: 'row',bottom:hp('.5%') }}>
                            <Text style={{
                                fontFamily:fontProperties.bold,
                                fontSize: hp('2.2%'),
                                color: '#94B5FC'
                            }}
                            >
                                See all
                            </Text>
                            <Ions
                                name="chevron-forward"
                                size={hp('3%')}
                                color={'#94B5FC'}
                            />
                        </View>
                    </View>
                </View>
                <View>
                    <Popular data={skills} navigation={props.navigation} />
                </View>
            </LinearGradient>
        </View>
    )
};



export default Japan;