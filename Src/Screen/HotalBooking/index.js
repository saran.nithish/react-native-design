import React from 'react';
import { StatusBar } from 'react-native';
import { View, Text, Image, ImageBackground } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globlaStyle/styles';
import styles from './styles';
/////library
import Font from 'react-native-vector-icons/FontAwesome5'
import Ions from 'react-native-vector-icons/Ionicons'
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DropShadow from "react-native-drop-shadow";
///components
import PopularPlace from '../../Components/PopularPlace';
import { imagepath } from '../../globlaStyle/imagePaths';
import globalColor from '../../globlaStyle/color';
import { TouchableOpacity } from 'react-native';
import LastPlace from '../../Components/LastPlace';
import Bestdeal from '../../Components/Bestdeal';


const HotalBooking = (props) => {
    const [skills, setskills] = React.useState([
        {
            id: '1',
            title: 'Job title',
            image: imagepath.home1,

        },
        {
            id: '2',
            title: 'Request Date: ',
            image: imagepath.home2,

        },

        {
            id: '3',
            title: 'Name',
            image: imagepath.home3,
        },

        {
            id: '4',
            title: 'Name',
            image: imagepath.home3,
        },
    ])

    return (

        <View style={[globlaStyle.global, styles.backgroundColor]}>
            <StatusBar translucent backgroundColor="transparent" />
            <ScrollView>
                <ImageBackground
                    source={imagepath.home3}
                    style={styles.intro}
                >
                    <View style={{ paddingHorizontal: hp('2%'), paddingVertical: hp('5%') }}>
                        <View style={styles.profileimageview}>
                            <Image source={imagepath.profile}
                                style={globlaStyle.profileimage} />
                        </View>
                        <View style={styles.introTextView}>
                            <Text style={styles.intoText}>
                                Where are you
                            </Text>
                            <Text style={styles.intoText}>
                                going next
                            </Text>
                        </View>

                    </View>
                </ImageBackground>
                <View style={{ bottom: hp('10%'), paddingHorizontal: hp('2%') }}>
                    <View style={styles.inputbackground}>
                        <View style={styles.inputSub}>
                            <View style={styles.inputStyle}>
                                <View style={globlaStyle.row}>
                                    <View style={styles.searchicon}>
                                        <Font
                                            name="search"
                                            size={hp('3%')}
                                            color={'#82D0CC'}

                                        />
                                    </View>
                                    <View style={styles.searchinput}>
                                        <TextInput
                                            placeholder='Try London,Cape Town'
                                            style={styles.textinput}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={[globlaStyle.row, { paddingVertical: hp('2%') }]}>
                            <View style={[globlaStyle.flex, { paddingHorizontal: hp('2%'), paddingVertical: hp('1%') }]}>
                                <Text style={styles.choose}>
                                    Choose date
                                </Text>
                                <Text style={styles.date}>
                                    12Dec - 22Dec
                                </Text>
                            </View>
                            <View style={{ height: '100%', width: hp('.15%'), backgroundColor: '#C2C2C2' }} />
                            <View style={[globlaStyle.flex, { paddingHorizontal: hp('2%'), paddingVertical: hp('1%') }]}>
                                <Text style={styles.choose}>
                                    Number of Rooms
                                </Text>
                                <Text style={styles.date}>
                                    1 Room - 2 Adults
                                </Text>
                            </View>
                        </View>
                        {/* // <View style={{ paddingTop: hp('2%') }}> */}
                        <TouchableOpacity style={styles.buttonStyle}>
                            <Text style={styles.btnText}>
                                Search Hotels
                            </Text>
                        </TouchableOpacity>
                        {/* </View> */}
                    </View>
                </View>
                <View style={{bottom:hp('5%')}}>
                    <View style={styles.subView}>
                        <Text style={styles.subtitle}>
                            Last searches
                        </Text>
                    </View>
                    <View style={styles.productview}>
                        <LastPlace data={skills} navigation={props.navigation} />
                    </View>
                    <View style={styles.subView2}>
                        <Text style={styles.subtitle}>
                            Popular Destinations
                        </Text>
                    </View>
                    <View style={styles.productview}>
                        <PopularPlace data={skills} navigation={props.navigation} />
                    </View>
                    <View style={styles.subView2}>
                        <Text style={styles.subtitle}>
                            Best Deals
                        </Text>
                    </View>
                    <View style={styles.productview}>
                        <Bestdeal data={skills} navigation={props.navigation} />
                    </View>
                </View>
            </ScrollView>
        </View>
    )
};



export default HotalBooking;