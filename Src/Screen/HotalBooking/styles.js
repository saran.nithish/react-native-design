import { StyleSheet } from 'react-native';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';
import globalColor from '../../globlaStyle/color'
import { color } from 'react-native-reanimated';

export default StyleSheet.create({
    backgroundColor:{
        backgroundColor:globalColor.white
    },
    intro:{
        width:'100%',
        height:hp('50%'),
        opacity:0.9
    },
    profileimageview:{
        alignItems:'flex-end',
       // right:hp('2%'),
        paddingTop:hp('2%')
    },
    intoText:{
        fontFamily:fontProperties.bold,
        fontSize:hp('4%'),
        color:globalColor.white,
      //  fontWeight:'bold'

    },
    introTextView:{
       // paddingVertical:hp('2%')
       // paddingHorizontal:hp('3%'),
        paddingTop:hp('13%')
    },
    inputbackground:{
        backgroundColor:'#fff',
        borderRadius:hp('5%'),
        paddingVertical:hp('1.8%'),
        paddingHorizontal:hp('2%'),
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    inputSub: {
      //  flex: 1,
        //justifyContent: 'center',
       // paddingHorizontal: hp('2%'),
       // bottom: hp('4%')
    },
    searchicon: {
        flex: 0.1,
        top: hp('1.5%')
    },
    searchinput: {
        flex: 1,
       // top: hp('.5%')
    },
    inputStyle: {
        height: hp('7%'),
        borderWidth: hp('.15%'),
        borderColor: '#CCCDD1',
        backgroundColor: '#fff',
        borderRadius: hp('3.4%'),
        paddingHorizontal: hp('2%'),
        justifyContent: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    buttonStyle: {
        height: hp('7%'),
        backgroundColor: '#54D3C2',
        borderRadius: hp('3.4%'),
        justifyContent: 'center',
        alignItems:'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    btnText:{
        fontFamily:fontProperties.bold,
        fontSize:hp('2%'),
        color:globalColor.white,
        //fontWeight:'bold'
    },
    subtitle:{
        fontFamily:fontProperties.bold,
        fontSize:hp('2.5%'),
        color:globalColor.black,
       // fontWeight:'bold'
    },
    subView:{
       // bottom:hp('8%'),
       // paddingTop:hp('15%'),
        paddingHorizontal:hp('2%')
    },
    subView2:{
       paddingTop:hp('2%'),
        paddingHorizontal:hp('2%'),
       // bottom:hp('8%'),
    },
    productview:{
        marginLeft:hp('1%'),
        paddingTop:hp('1%')
       // bottom:hp('8%'),
    },
    choose:{
        fontSize:hp('1.5%'),
        color: '#C2C2C2',
        fontFamily:fontProperties.regular
    },
    date:{
        fontSize:hp('1.5%'),
        fontFamily:fontProperties.regular,
        color: '#333'
    }
});
