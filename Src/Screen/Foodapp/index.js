import React from 'react';
import { StatusBar } from 'react-native';
import { View, Text, TouchableOpacity, Image } from 'react-native';
/////library
import { ScrollView, Switch, } from 'react-native-gesture-handler';
import { fontProperties } from '../../globlaStyle/font';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import DropShadow from "react-native-drop-shadow";
import LinearGradient from 'react-native-linear-gradient';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import { withTheme } from 'react-native-paper';
import { EventRegister } from 'react-native-event-listeners';
///styles
import styles from './styles';
import globlaStyle from '../../globlaStyle/styles';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
///Components
import Today from './single';
import Pizza from './todayspecial';
import CoolDrink from './combo';
import imagepath from '../../globlaStyle/imagePaths';


const Foodapp = ({ theme, children }) => {

    const { colors } = theme;
    const Tab = createMaterialTopTabNavigator();
    const [isDarkTheme, setIsDarkTheme] = React.useState(false);
    const [skills, setskills] = React.useState([
        {
            id: '1',
            title: 'Burgers',
            image:imagepath.Burgers,

        },
        {
            id: '2',
            title: 'Pizza ',
            image: imagepath.Pizza

        },

        {
            id: '3',
            title: 'Drinks',
            image: imagepath.Drink,
        },
    ])

    return (
        <LinearGradient colors={[colors.background, colors.background2]}>
            <View style={globlaStyle.global}>
                <StatusBar backgroundColor={colors.background} />
                <View style={styles.subcontainer}>
                    <View style={globlaStyle.row}>
                        <View style={globlaStyle.flex}>
                            <View style={styles.welview}>
                                <Text style={[styles.wel, { color: colors.text }]}>{'Welcome'}</Text>
                            </View>
                            <View style={styles.nameView}>
                                <Text style={[styles.name, { color: colors.text }]}>{'Aleta Oscar'}</Text>
                                <Entypo
                                    name="dot-single"
                                    size={hp('3%')}
                                    color={'#33B250'}
                                    style={{ right: hp('1%') }}
                                />
                            </View>
                        </View>
                        <View style={styles.menuView}>
                            <View style={[styles.menu, { backgroundColor: colors.background }]}>
                                <TouchableOpacity>
                                    <Feather
                                        name="align-center"
                                        size={hp('3%')}
                                        color={colors.menuicon}
                                    ///style={{ left: hp('3%'), }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <Tab.Navigator tabBarOptions={{
                        allowFontScaling: false,
                        indicatorStyle: {
                            height: 10,
                            width: 10,
                            left: '15%',
                            marginBottom: 2,
                            borderRadius: 10,
                            bottom: hp('2.5%'),
                            alignItems: 'center',
                            backgroundColor: '#FF4921',
                        },

                        labelStyle: {
                            fontSize: 11,
                            fontFamily: fontProperties.semiBold,
                            color: colors.text
                        },
                        style: {
                            backgroundColor: colors.background,
                            elevation: 0, // remove shadow on Android
                            shadowOpacity: 0,
                            paddingVertical: hp('2%')
                        },
                    }} >
                        <Tab.Screen name="Singles" component={Today}
                            options={{
                                tabBarLabel: 'Singles'
                            }} />
                        <Tab.Screen name="Today Special" component={Pizza}
                            options={{
                                tabBarLabel: 'Today Special'
                            }} />
                        <Tab.Screen name="Combo" component={CoolDrink}
                            options={{
                                tabBarLabel: 'Combo'
                            }} />
                    </Tab.Navigator>
                    <View style={styles.upperTabView}>
                        <ScrollView
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                        >
                            <View style={globlaStyle.row}>
                                {skills.map(item => {
                                    return (
                                        <DropShadow style={styles.dropShaw} >
                                            <View style={styles.rowbox}>
                                                <Text style={[styles.foodtext, { color: colors.text }]}>{item.title}</Text>
                                                <View style={styles.imgboxview}>
                                                    <View style={[styles.imgbox, { backgroundColor: colors.background }]} >
                                                        <Image source={item.image}
                                                            style={styles.imgstyle} />
                                                    </View>
                                                </View>
                                            </View>
                                        </DropShadow>
                                    )
                                })
                                }
                            </View>
                            <View style={styles.switchView}>
                                <Switch
                                    value={isDarkTheme}
                                    onValueChange={val => {
                                        setIsDarkTheme(val);
                                        EventRegister.emit('changeThemeEvent', val)
                                    }}
                                />
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </View>
        </LinearGradient>
    )
};

export default withTheme(Foodapp);