import React from 'react';
import { View, Text, Image } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import styles from './styles';
import globalColor from '../../globlaStyle/color';
//////library
import Entypo from 'react-native-vector-icons/Entypo';
import Font from 'react-native-vector-icons/FontAwesome'
import DropShadow from 'react-native-drop-shadow';
import LinearGradient from 'react-native-linear-gradient';
import { withTheme } from 'react-native-paper';
import imagepath from '../../globlaStyle/imagePaths';




const Today = ({ theme, children }) => {

    const { colors } = theme;

    return (
        <LinearGradient colors={[colors.background, colors.background2]}>
            <View style={styles.pizzaView}>
                <DropShadow style={styles.dropShaw} >
                    <View style={styles.box}>
                        <View style={styles.imgview}>
                            <View style={styles.imgsub}>
                                <View style={styles.cooldrinkImage}>
                                    <Image
                                        source={imagepath.Drink}
                                        style={styles.image}
                                    />
                                </View>
                            </View>
                            <View style={styles.heartIcon}>
                                <DropShadow style={styles.dropShaw} >
                                    <View style={styles.heartCircle}>
                                        <Font
                                            name="heart"
                                            size={hp('2.5%')}
                                            color={globalColor.Iconcolor}
                                        />
                                    </View>
                                </DropShadow>
                            </View>
                        </View>
                        <View style={styles.coolDrinktitle}>
                            <View style={styles.cooldrinktitleText}>
                                <Text style={[styles.titleText, { color: colors.text }]}>
                                   {'Apple Rush'} 
                        </Text>
                                <Text style={[styles.desText, { color: colors.text }]}>
                                    Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry
                                    Lorem Ipsum has been the industry's standard dummy text ever since
                                    the 1500s when an unknown printer took a galley of type and scrambled it to
                        </Text>
                            </View>
                            <View style={styles.addbtnView}>
                                <View style={[styles.addBtn, { backgroundColor: colors.addbt }]}>
                                    <Entypo
                                        name="plus"
                                        size={hp('2.5%')}
                                        color={globalColor.Iconcolor}
                                    />
                                </View>
                                <View style={styles.numberview}>
                                    <Text style={[styles.countText, { color: colors.text }]}>2</Text>
                                </View>
                                <View style={styles.mineBtn}>
                                    <View style={[styles.addBtn, { backgroundColor: colors.addbt }]}>
                                        <Entypo
                                            name="minus"
                                            size={hp('2.5%')}
                                            color={globalColor.Iconcolor}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.buybtn, { backgroundColor: colors.buynow }]}>
                            <Text style={[styles.buynowtext, { color: colors.buytext }]}>Buy Now</Text>
                        </View>
                    </View>
                </DropShadow>
            </View>
        </LinearGradient>
    )
};



export default withTheme(Today);
