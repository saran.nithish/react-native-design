import { StyleSheet } from 'react-native';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';


export default StyleSheet.create({

    subcontainer: {
        flex: 1,
        padding: hp('1%')
    },
    nameView: {
        bottom: hp('1%'),
        flexDirection: 'row',
        marginLeft: hp('1%')
    },
    welview: {
        marginLeft: hp('1%')
    },
    wel: {
        fontFamily: fontProperties.regular,
        fontSize: hp('3%')
    },
    name: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('3.5%'),
        color: '#333'
    },
    menuView: {
        flex: 1,
        alignItems: 'flex-end',
        paddingTop: hp('2%'),
    },
    switchView: {
        alignItems: 'center'
    },
    imgstyle: {
        width: hp('5%'),
        height: hp('5%')
    },
    imgboxview: {
        paddingTop: hp('2%')
    },
    rowbox: {
        paddingHorizontal: hp('1.5%'),
    },
    todaytitleView: {
        flex: 1.5,
        marginLeft: '4%'
    },
    coolDrinktitle: {
        flexDirection: 'row',
        bottom: hp('18%')
    },
    cooldrinktitleText: {
        flex: 1.5,
        marginLeft: '4%'
    },
    pizzaimageView:{
        height: '70%',
         width: '100%'
    },
    cooldrinkImage: {
        height: '70%'
    },
    upperTabView: {
        bottom: hp('3%')
    },
    dotStyle:{
        height: 10,
        width: 10,
        ///position: '',
        left: '15%',
        marginBottom: 2,
        borderRadius: 10,
        //marginLeft:30,
        bottom: hp('2.5%'),
        alignItems: 'center',
        backgroundColor: '#FF4921',

    },
    tabstyles:{
        elevation: 0, // remove shadow on Android
        shadowOpacity: 0,
        paddingVertical: hp('2%')
    },
    ////////
    pizzaView: {
        width: '100%',
        height: '100%',
        //backgroundColor: '#EBF3FA',
        paddingVertical: hp('9%')
    },
    pizzaimage: {
        height: '70%',
    },
    dropShaw: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
    },
    box: {
        height: '100%',
        transform: [{ scaleX: 0.9 }],
        borderBottomStartRadius: hp('5%'),
        borderBottomEndRadius: hp('5%'),
        borderTopStartRadius: hp('5%'),
        borderTopEndRadius: hp('5%'),
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    numberview: {
        paddingVertical: hp('.5%')
    },
    mineBtn: {
        bottom: hp('1%')
    },
    titleTextView: {
        flex: 1.5,
        marginLeft: '4%'
    },
    imgview: {
        flexDirection: 'row',
        margin: -10,
        zIndex: 100,
        bottom: hp('10%')
    },
    imgsub: {
        flex: 1,
        marginLeft: hp('2%')
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'stretch'
    },
    heartIcon: {
        flex: 1,
        paddingVertical: hp('15%'),
        alignItems: 'flex-end',
        right: hp('9%')
    },
    heartCircle: {
        height: hp('6%'),
        width: hp('6%'),
        borderRadius: hp('6%'),
        backgroundColor: "#FF4921",
        alignItems: 'center',
        justifyContent: 'center',
    },
    addBtn: {
        height: hp('5%'),
        width: hp('5%'),
        borderRadius: hp('5%'),
        //backgroundColor: "#A1B8CF",
        alignItems: 'center',
        justifyContent: 'center',
    },
    buybtn: {
        height: hp('7%'),
        width: hp('22%'),
        // backgroundColor: '#1B1A21',
        position: 'absolute',
        justifyContent: 'center',
        zIndex: 99,
        top: '90%',
        left: '50%',
        marginLeft: '4%',
        borderTopEndRadius: hp('7%'),
        borderBottomLeftRadius: hp('5%'),
        borderTopEndRadius: hp('5%'),
        borderBottomRightRadius: hp('5%'),
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 20,
    },
    buynowtext: {
        textAlign: 'center',
        fontFamily: fontProperties.Medium,
        fontSize: hp('2.5%'),
    },
    titleView: {
        flexDirection: 'row',
        bottom: hp('20%')
    },
    titleText: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('3%')
    },
    desText: {
        fontFamily: fontProperties.Medium,
        fontSize: hp('1.5%'),
        color: '#828790'
    },
    addbtnView: {
        flex: 0.5,
        alignItems: 'center',
        paddingVertical: hp('1.5%')
    },
    countText: {
        fontSize: hp('3%'),
        fontFamily: fontProperties.semiBold
    },
    foodtext: {
        textAlign: 'center',
        fontSize: hp('2%'),
        fontFamily: fontProperties.semiBold
    },
    menu: {
        height: hp('7%'),
        width: hp('7%'),
        borderRadius: hp('7%'),
        // backgroundColor: "#EBF3FA",
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 10,
    },
    imgbox: {
        width: hp('13%'),
        height: hp('12%'),
        bottom: hp('1%'),
        borderRadius: hp('3%'),
        alignItems: 'center',
        justifyContent: 'center'
    }
});