import { StyleSheet } from 'react-native';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';
import globalColor from '../../globlaStyle/color'

export default StyleSheet.create({
    backgroundColor: {
        backgroundColor: globalColor.white
    },
    header: {
        height: hp('20%'),
        backgroundColor: globalColor.primaryColor
    },
    headerinnerview: {
        flexDirection: 'row',
        padding: hp('3%'),
        top: hp('3%')
    },

    inputMain: {
        height: hp('10%'),
        width: '100%',
        paddingHorizontal: hp('1.5%')
    },
    inputSub: {
        flex: 1,
        //justifyContent: 'center',
        paddingHorizontal: hp('2%'),
        bottom: hp('4%')
    },
    inputStyle: {
        height: hp('7%'),
        borderWidth: hp('.15%'),
        borderColor: '#CCCDD1',
        backgroundColor: '#fff',
        borderRadius: hp('3.4%'),
        paddingHorizontal: hp('2%'),
        justifyContent: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    circleShap: {
        height: hp('4%'),
        width: hp('4%'),
        borderRadius: hp('2.5%'),
        backgroundColor: "#FFCD1C",
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleShap2: {
        elevation: 4,
        position: "absolute",
        zIndex: 100,
        marginLeft: hp('1%'),
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp('1%'),
        height: hp('2.5%'),
        width: hp('2.5%'),
        borderRadius: hp('1.5%'),
        backgroundColor: 'rgba(52, 52, 52, 0.5)',

    },
    checkBtn: {
        backgroundColor:globalColor.primaryColor,
        padding: hp('1.5%'),
        width: hp('20%'),
        borderRadius: hp('2%'),
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    innermain: {
        backgroundColor: '#F9F9F9',
        borderRadius: hp('2%'),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    innersub: {
        //  marginLeft: hp('3%'), 
        /// padding: hp('.5%')
    },

    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
    },
    imageText: {
        position: 'absolute',
        zIndex: 100,
        fontFamily: fontProperties.semiBold,
        marginLeft: hp('3.5%'),
        top: hp('21.5%'),
        color: '#fff'
    },
    imagestyle: {
        width: hp('16%'),
        height: hp('25%'),
        borderRadius: hp('3%')
    },
    productimage: {
        width: hp('22%'),
        height: hp('15%'),
    },
    productimageView: {
        flex: 1,
        right: hp('1.5%'),
        borderBottomColor: globalColor.bottomColor,
        borderBottomWidth: hp('.15%'),
        paddingVertical: hp('1%')
    },
    profileimageview: {
        flex: 0.2
    },
    nameView: {
        flex: 0.6,
        justifyContent: 'center'
    },
    profiletext: {
        fontSize: hp('2%'),
        fontFamily: fontProperties.semiBold,
        color: '#000'
    },
    belliconview: {
        flex: 0.2,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    hotproductview: {
        flex: 1,
        borderBottomColor: globalColor.bottomColor,
        borderBottomWidth: hp('.15%'),
        paddingVertical: hp('1%')
    },
    producttitle: {
        fontSize: hp('2%'),
        fontFamily: fontProperties.Medium
    },
    priceText: {
        fontSize: hp('1.8%'),
        fontFamily: fontProperties.regular
    },
    addressText: {
        fontSize: hp('1.5%'),
        color: '#858585',
        fontFamily: fontProperties.regular
    },
    addressView: {
        paddingVertical: hp('.2%')
    },
    rateText: {
        fontSize: hp('1.8%'),
        fontFamily: fontProperties.regular
    },
    iconstyle: {
        marginTop: hp('.3%')
    },
    heartView: {
        flexDirection: 'row',
        padding: hp('1%')
    },
    cartstyle: {
        marginLeft: hp('1%')
    },
    carticonstyle: {
        right: hp('.1%')
    },
    searchicon: {
        flex: 0.1,
        top: hp('1.5%')
    },
    searchinput: {
        flex: 1,
        top: hp('.5%')
    },
    textinput: {
        fontFamily: fontProperties.regular,
        fontSize: hp('1.8%')
    },
    promoview: {
        flex: 1.5,
        marginLeft: hp('3%'),
    },
    promotitleview: {
        bottom: hp('2%')
    },
    hottitleview: {
        paddingTop: hp('5%')
    },
    promobox: {
        paddingHorizontal: hp('1.5%'),
        right: hp('1.6%')
    },
    productview: {
        paddingVertical: hp('4%')
    },
    starviewText: {
        marginLeft: hp('1%')
    },
    /////////cartstyle////////
    subCont: {
        flex: 1,
        margin: hp('2%')
    },
    carttitleview: {
        paddingVertical: hp('1.5%')
    },
    boxstyle: {
        flex: 1,
        paddingBottom: hp('1.8%'),
        paddingHorizontal: hp('1%')
    },
    cartimage: {
        width: hp('20%'),
        height: hp('18%'),
        borderRadius: hp('2%')
    },
    cartimgview: {
        flex: 1.2
    },
    cartpricetext: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('1.8%')
    },
    cartpriceView: {
        marginLeft: hp('1%'),
        top: hp('1.5%')
    },
    addno: {
        fontFamily: fontProperties.semiBold,
        top: hp('.2%')
    },
    carttitle: {
        fontSize: hp('2.2%'),
        fontFamily: fontProperties.Medium
    },
    cartcolortext: {
        color: '#333',
        fontSize: hp('1.4%'),
        fontFamily: fontProperties.bold, bottom: hp('.3%')
    },
    cartproductview: {
        flex: 1,
        right: hp('2%'),
    },
    cartproducttitleview: {
        paddingVertical: hp('.5%'),
        marginLeft: hp('.5%')
    },
    checkBtnView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    checkbtntext: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('2.5%'),
        textAlign: 'center'
    },
    totalpricetext: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('3%')
    },
    totaltitle: {
        color: '#B2B2B2',
        fontFamily: fontProperties.Medium,
        fontSize: hp('2%')
    },
    totalpriceview: {
        bottom: hp('1%')
    },
    productline: {
        borderBottomWidth: hp('.15%'),
        borderBottomColor: '#CFCFCF',
        paddingVertical: hp('1%')
    },
    foot: {
        flexDirection: 'row',
        top: hp('2%')
    },
    footer: {
        borderTopWidth: hp('.15%'),
        borderTopColor: '#CFCFCF',
        margin: hp('2%'),
        bottom: hp('1%')
    },
    addbtntext: {
        justifyContent: 'center',
        marginLeft: hp('.5%')
    },
    addbtnview: {
        backgroundColor:globalColor.primaryColor,
        width: hp('3%'),
        alignItems: 'center',
        borderRadius: hp('1%')
    },
    mineiconview:{
        marginLeft: hp('.5%')
    },
    tabstyles:{
        backgroundColor: '#fff',
        borderTopWidth:2,
        borderTopColor:'#E7E7E7'
    }

});
