import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { View, Text, TouchableOpacity, FlatList, TouchableHighlight, Image } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
//import Icon from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import Entypo from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/Ionicons';
import globlaStyle from '../../globlaStyle/styles';
import globalColor from '../../globlaStyle/color';

const DATA = [
    {
        id: '1',
        title: 'Job title',
        image: require('../../../assets/image/dinnerset2.jpg'),

    },
    {
        id: '2',
        title: 'Request Date: ',
        image: require('../../../assets/image/dinnerset.jpg'),
    },


];


const HomeScreen = () => {
    const [skills, setskills] = React.useState([
        {
            id: '1',
            title: 'Job title',


        },
        {
            id: '2',
            title: 'Request Date: ',

        },

        {
            id: '3',
            title: 'Name',

        },
    ])

    return (

        <View style={[globlaStyle.global,styles.backgroundColor]}>
            <StatusBar translucent backgroundColor="transparent" />
            <ScrollView>
                <View style={styles.subCont}>
                    <View style={styles.carttitleview}>
                        <Text style={globlaStyle.title}>Cart</Text>
                    </View>
                    <FlatList
                        data={DATA}
                        keyExtractor={item => item.id}
                        renderItem={({ item, index }) => {
                            return (
                                <View style={styles.boxstyle}>
                                    <View style={styles.innermain}>
                                        <View style={styles.innersub}>
                                            <View style={globlaStyle.row}>
                                                <View style={styles.cartimgview}>
                                                    <Image
                                                        source={item.image}
                                                        style={styles.cartimage} />
                                                    <View style={styles.circleShap2}>
                                                        <Entypo
                                                            name="cross"
                                                            size={hp('2%')}
                                                            color={globalColor.white}
                                                            style={styles.webicon2}
                                                        />
                                                    </View>
                                                </View>
                                                <View style={styles.cartproductview}>
                                                    <View style={styles.productline}>
                                                        <View style={styles.cartproducttitleview}>
                                                            <Text style={styles.carttitle}>Table Set</Text>
                                                            <Text style={styles.cartcolortext}> Color:Brown</Text>
                                                        </View>
                                                       
                                                        <View style={globlaStyle.row}>
                                                            <View>
                                                                <Icon
                                                                    name="chevron-back-outline"
                                                                    size={hp('3%')}
                                                                    color={globalColor.black}
                                                                   
                                                                />
                                                            </View>
                                                            <View style={styles.addbtntext}>
                                                                <View style={styles.addbtnview}>
                                                                    <Text style={styles.addno}>
                                                                        2
                                                </Text>
                                                                </View>
                                                            </View>
                                                            <View style={styles.mineiconview}>
                                                                <Icon
                                                                    name="chevron-forward-outline"
                                                                    size={hp('3%')}
                                                                    color={globalColor.black}
                                                                  
                                                                />
                                                            </View>
                                                        </View>
                                                    </View>
                                                    <View style={styles.cartpriceView}>
                                                        <Text style={styles.cartpricetext}>Rp 2.4440.00</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            )
                        }}
                    />
                </View>
            </ScrollView>
            <View style={styles.footer}>
                <View style={styles.foot}>
                    <View style={globlaStyle.flex}>
                        <View>
                            <Text style={styles.totaltitle}>
                                Total Price
                    </Text>
                        </View>
                        <View style={styles.totalpriceview}>
                            <Text style={styles.totalpricetext}>
                                Rp 3.589.000
                    </Text>
                        </View>
                    </View>
                    <View style={styles.checkBtnView}>
                        <TouchableOpacity>
                            <View style={styles.checkBtn}>
                                <Text style={styles.checkbtntext}>
                                    Checkout
                    </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        </View>
    )
};



export default HomeScreen;
