import React from 'react';
import { StatusBar } from 'react-native';
import { View, Text, Image } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globlaStyle/styles';
import styles from './styles';
/////library
import Font from 'react-native-vector-icons/FontAwesome'
import Ions from 'react-native-vector-icons/Ionicons'
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DropShadow from "react-native-drop-shadow";
///components
import ProductList from '../../Components/Productlist';
import {imagepath} from '../../globlaStyle/imagePaths';
import globalColor from '../../globlaStyle/color';


const HomeScreen = (props) => {
    const [skills, setskills] = React.useState([
        {
            id: '1',
            title: 'Job title',
            image:imagepath.chair3,

        },
        {
            id: '2',
            title: 'Request Date: ',
            image:imagepath.dinnerset,

        },

        {
            id: '3',
            title: 'Name',
            image:imagepath.chair4,
        },
    ])

    return (

        <View style={[globlaStyle.global,styles.backgroundColor]}>
            <StatusBar backgroundColor={globalColor.primaryColor} />
            <ScrollView>
                <View style={styles.header}>
                    <View style={styles.headerinnerview}>
                        <View style={styles.profileimageview}>
                            <Image source={imagepath.profile}
                                style={globlaStyle.profileimage} />
                        </View>
                        <View style={styles.nameView}>
                            <Text style={styles.profiletext}>
                                {'Hi, Joe'}
                            </Text>
                        </View>
                        <View style={styles.belliconview}>
                            <Font
                                name="bell"
                                size={hp('3%')}
                                color={globalColor.bellcolor}
                                style={styles.webicon2}
                            />
                        </View>
                    </View>
                </View>
                <View style={styles.inputSub}>
                    <View style={styles.inputStyle}>
                        <View style={globlaStyle.row}>
                            <View style={styles.searchinput}>
                                <TextInput
                                    placeholder='Search'
                                    style={styles.textinput}
                                />
                            </View>
                            <View style={styles.searchicon}>
                                <Ions
                                    name="md-search-sharp"
                                    size={hp('3%')}
                                    color={'#8D8D8D'}

                                />
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.promoview}>
                    <View style={styles.promotitleview}>
                        <Text style={globlaStyle.title}>{'Promo'}</Text>
                    </View>
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                    >
                        <DropShadow
                            style={styles.shadow}
                        >
                            <View style={globlaStyle.row}>
                                {skills.map(item => {
                                    return (
                                        <View style={styles.promobox}>
                                            <Image source={item.image}
                                                style={styles.imagestyle} />
                                            <Text style={styles.imageText}>
                                                {'Silver Family'}
                                            </Text>
                                        </View>
                                    )
                                })
                                }
                            </View>
                        </DropShadow>
                    </ScrollView>
                    <View style={styles.hottitleview}>
                        <Text style={globlaStyle.title}>{'Hot Products'}</Text>
                    </View>
                    <View>
                        <View style={styles.productview}>
                                <ProductList data={skills} navigation={props.navigation}/>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
};



export default HomeScreen;