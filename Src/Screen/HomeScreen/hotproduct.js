import React  from 'react';
import { StatusBar } from 'react-native';
import { View, Text} from 'react-native';
import { ScrollView,} from 'react-native-gesture-handler';
////styles
import globlaStyle from '../../globlaStyle/styles';
import styles from './styles';
import {imagepath} from '../../globlaStyle/imagePaths';
///components
import ProductList from '../../Components/Productlist'


const Hotproduct = (props) => {
    const [skills, setskills] = React.useState([
        {
            id: '1',
            title: 'Job title',
            image:imagepath.chair3,

        },
        {
            id: '2',
            title: 'Request Date: ',
            image:imagepath.dinnerset,

        },
    ])
   

    return (
        <View style={[globlaStyle.global,styles.backgroundColor]}>
            <StatusBar  translucent backgroundColor="transparent" />
            <ScrollView>
                <View style={styles.promoview}>
                    <View style={styles.hottitleview}>
                        <Text style={globlaStyle.title}>{'WishList'}</Text>
                    </View>
                    <View>
                        <View style={styles.productview}>
                                <ProductList data={skills} navigation={props.navigation}/>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
};

export default Hotproduct;