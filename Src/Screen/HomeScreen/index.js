import React from 'react';
////library
import Icon from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HotProduct from './hotproduct';
import Home from './home';
import Cart from './cart';

const HomeScreen = () => {
    const Tab = createBottomTabNavigator();

    return (
        <Tab.Navigator 
            swipeEnabled={true} tabBarOptions={{
                tabStyle: {
                backgroundColor: '#fff',
                borderTopWidth:2,
                borderTopColor:'#E7E7E7'
                },
                backgroundColor: '#fff',
                activeTintColor: '#FFCD1C',
                inactiveTintColor: '#000',
                inactiveColor: '#000',
                showLabel: false,
            }}
        >
            <Tab.Screen name="Home" component={Home}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Icon name="ios-home-outline" color={color} size={25} />
                    ),
                }}
            />
            <Tab.Screen name="Menu" component={HotProduct} options={{
                tabBarIcon: ({ color }) => (
                    <Icon name="grid-outline" color={color} size={25} />
                ),
            }} />
            <Tab.Screen name="Cart" component={Cart} options={{
                tabBarIcon: ({ color }) => (
                    <Icon name="md-cart-outline" color={color} size={27} />
                ),
            }} />
        </Tab.Navigator>

    )
};


export default HomeScreen;