import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';
import globalColor, { colors } from '../../globlaStyle/color'

export default StyleSheet.create({
    maincont: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
    },
    subCont: {
        flex: 1,
        padding: hp('1.5%'),
        opacity:0.85
    },
    inputMain: {
        height: hp('10%'),
        width: '100%',
        paddingHorizontal: hp('1.5%')
    },
    inputSub: {
        flex: 1,
        paddingHorizontal: hp('2%'),
        bottom: hp('4%')
    },
    inputStyle: {
        height: hp('7%'),
        borderWidth: hp('.15%'),
        borderColor: '#CCCDD1',
        backgroundColor: '#fff',
        borderRadius: hp('3.4%'),
        paddingHorizontal: hp('2%'),
        justifyContent: 'center',
    },
    circleShap: {
        height: hp('8%'),
        width: hp('8%'),
        borderRadius: hp('8%'),
        backgroundColor: "#ffff",
        alignItems: 'center',
        justifyContent: 'center',
        //shadowColor:colors.black,
     //   elevation: 5,
        //borderWidth: hp('.20%')
    },
    circleShap2: {
        height: hp('8%'),
        width: hp('8%'),
        borderRadius: hp('8%'),
        backgroundColor: "#333",
        alignItems: 'center',
        justifyContent: 'center',
       // shadowColor:colors.black,
       // elevation: 5,
        //borderWidth: hp('.20%')
    },
    circleShap3: {
        height: hp('2%'),
        width: hp('2%'),
        borderRadius: hp('1.5%'),
        backgroundColor: "#686868",
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleShap4: {
        height: hp('3%'),
        width: hp('3%'),
        borderRadius: hp('1.5%'),
       
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleline: {
        marginTop: hp('1.2%'),
        width: wp('80%'),
        height: hp('.15%'),
        backgroundColor: '#D7D7D7'
    },

    checkBtn: {
        backgroundColor: globalColor.primaryColor,
        padding: hp('1%'),
        borderRadius: hp('2%'),
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    curveShape: {
        height: hp('45%'),
        transform: [{ scaleX: 1.5 }],
        borderBottomStartRadius: 200,
        borderBottomEndRadius: 200,
        overflow: 'hidden',
    },
    img: {
        width: '100%',
        height: '100%',
    },
    contentview: {
        paddingVertical: hp('1%'),
        flexDirection: 'row'
    },
    contentbox: {
        flex: 1.5,
        padding: hp('3%')
    },
    pirceText: {
        fontFamily: fontProperties.semiBold,
        fontWeight:'bold',
        fontSize: hp('2%'),
        textAlign: 'center',
        color: globalColor.black,
        top:hp('2%')
    },
    pirceText2: {
        fontFamily: fontProperties.regular,
        fontSize: hp('1.8%'),
        //textAlign: 'center',
        color: globalColor.black,
        top:hp('2%')
    },
    
    pirceview: {
        backgroundColor: globalColor.primaryColor,
        padding: hp('.5%'),
        
    },
    priceView2: {
        marginLeft: hp('4%'),
        justifyContent: 'center'
    },
    starView: {
        flexDirection: 'row',
        paddingVertical: hp('2%')
    },
    rateTextview: {
        marginLeft: hp('2%')
    },
    colorText: {
        fontSize: hp('2%'),
        fontWeight: 'bold'
    },
    circleView: {
        marginLeft: hp('1%'),
        justifyContent: 'center'
    },
    desView: {
        paddingVertical: hp('1%')
    },
    addtext: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('2.5%'),
        color: globalColor.black
    },
    addTextView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    title:{
        fontSize:hp('3.5%'),
        fontFamily:fontProperties.bold,
        fontWeight:'bold'
    },
    title2:{
        fontSize:hp('2%'),
        fontFamily:fontProperties.regular,
        color:colors.black,

    },
    myStarStyle: {
        color: 'black',
        backgroundColor: 'transparent',
       // textShadowColor: 'black',
      //  textShadowOffset: {width: 1, height: 1},
       // textShadowRadius: 2,
      },
      myEmptyStarStyle: {
        color: 'white',
        //backgroundColor:'white'
      }
});
