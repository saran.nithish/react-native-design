import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { View, Text, TouchableOpacity, Image, ScrollView, ImageBackground } from 'react-native';
////////library
import Font from 'react-native-vector-icons/FontAwesome'
import StarRating from 'react-native-star-rating';
import Icon from 'react-native-vector-icons/Ionicons';

import Mat from 'react-native-vector-icons/MaterialCommunityIcons';
import Ant from 'react-native-vector-icons/AntDesign';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
///styles
import styles from './styles';
import globlaStyle from '../../globlaStyle/styles';
import globalColor from '../../globlaStyle/color';
import imagepath from '../../globlaStyle/imagePaths';
import Stars from 'react-native-stars';

const FashionScreen = () => {

    const [Star, setStar] = React.useState()

    const onStarRatingPress = (rating) => {
        setStar(rating)
    }

    return (

        <View style={styles.maincont}>
            <StatusBar     />
            <ImageBackground
                source={imagepath.fash}
                style={styles.subCont}
            >

                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                        <TouchableOpacity>
                            <Icon
                                name="chevron-back-outline"
                                size={hp('5%')}
                                color={globalColor.black}
                                style={styles.hearticonstyle}
                            />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity>
                            <Icon
                                name="ios-share-social-sharp"
                                size={hp('5%')}
                                color={globalColor.black}
                                style={styles.hearticonstyle}
                            />
                        </TouchableOpacity>
                    </View>
                </View>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ paddingVertical: hp('55%') }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={[globlaStyle.flex, { paddingHorizontal: hp('2%') }]}>
                                <Text style={styles.title}>
                                    Classic
                                </Text>
                                <Text style={styles.title}>
                                    Cotton Jacket
                                </Text>
                                <View style={{alignItems:'flex-start'}}>
                                <Stars
                                    default={3}
                                    count={5}
                                    //half={true}
                                   // starSize={10000}
                                    fullStar={<Mat name={'star'} size={20} style={[styles.myStarStyle]} />}
                                    emptyStar={<Mat name={'star-outline'} size={20}  style={[styles.myStarStyle, styles.myEmptyStarStyle]} />}
                                    halfStar={<Mat name={'star-half'} style={[styles.myStarStyle]} />}
                                />
                                </View>
                            </View>
                            <View style={{ flex: 0.5, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={styles.pirceText}>
                                    $208.99
                                </Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={[globlaStyle.flex, { paddingHorizontal: hp('2%'), paddingTop: hp('1%') }]}>
                                <Text style={styles.title2}>Color</Text>
                                <View style={{ flexDirection: 'row', paddingTop: hp('1%') }} >
                                    <View style={[styles.circleShap4, { backgroundColor: '#161616' }]} />
                                    <View style={{ marginLeft: hp('2%') }}>
                                        <View style={[styles.circleShap4, { backgroundColor: '#676767' }]} />
                                    </View>
                                    <View style={{ marginLeft: hp('2%') }}>
                                        <View style={[styles.circleShap4, { backgroundColor: '#9C9583' }]} />
                                    </View>
                                </View>
                                <View>
                                    <Text style={styles.pirceText2}>
                                        Lorem Ipsum is simply dummy text of the
                                        printing and typesetting industry.
                                        Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s,
                                    </Text>
                                </View>
                            </View>
                            <View style={{ flex: 0.5, alignItems: 'center', justifyContent: 'center' }}>
                                <View>
                                    <TouchableOpacity>
                                        <View style={styles.circleShap}>
                                            <Font
                                                name="heart"
                                                size={hp('2.5%')}
                                                color={globalColor.heartcolor}
                                                style={styles.iconstyle}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ paddingTop: hp('2%') }}>
                                    <TouchableOpacity>
                                        <View style={styles.circleShap2}>
                                            <Font
                                                name="shopping-bag"
                                                size={hp('2.5%')}
                                                color={globalColor.white}
                                                style={styles.iconstyle}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>





                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    )
};

export default FashionScreen;