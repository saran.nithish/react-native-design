import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';
import globalColor, { colors } from '../../globlaStyle/color'

export default StyleSheet.create({
    maincont: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
    },
    subCont: {
        flex: 1,
        padding: hp('1.5%'),
        opacity:0.85
    },
        image: {
          width: 150,
          height: 100,
          borderRadius: 13,
          margin: 3,
          resizeMode: 'cover',
        },
    
   
});
