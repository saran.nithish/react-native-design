import React, { Component } from 'react';
import { GiftedChat, Send, InputToolbar, Actions } from 'react-native-gifted-chat';
import Font from 'react-native-vector-icons/FontAwesome'
import { View, Text, Image, Dimensions } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import styles from './styles';
import { Icon } from 'react-native-elements';
import Ions from 'react-native-vector-icons/Ionicons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { TouchableOpacity } from 'react-native';
import NavigationBar from "react-native-navbar";

const options = {
  title: 'Select Image ',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
    mediaType: 'photo',
    includeBase64: false,
    maxHeight: 200,
    maxWidth: 200,
  },
};



class ChatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { messages: [] };
    this.onSend = this.onSend.bind(this);
  }

  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
          image: 'https://placeimg.com/140/140/any'
        },
      ],
    });
  }

  selectFile = () => {
    launchImageLibrary(options, (response) => { // Use launchImageLibrary to open image gallery
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
       

        //  console.log(source)
      }
    });

  }
  renderSend = (props) => (
    <Send {...props} containerStyle={{
      borderWidth: 0,
      padding: 1,
      // alignItems: 'center',
      borderTopWidth: 0.1,

      //      paddingVertical:hp('1%')
      marginBottom: 0
    }}>
      <View style={{ justifyContent: 'center', }}>
        <Font
          name="bell"
          size={hp('3%')}
          color={'black'}
          style={styles.webicon2}
        />
      </View>
    </Send>
  );

  customInputToolbar = (props) => {
    return (



      <InputToolbar
        {...props}
        containerStyle={styles.typeSec}
        textStyle={styles.typeSpaceTxt}
      />


    );
  };
  renderMessageImage(props) {
    const images = [{
      // Simplest usage.
      url: props.currentMessage.image,
      // You can pass props to <Image />.
      props: {
        // headers: ...
      }
    }, {
      props: {
        // Or you can set source directory.
        //  source: require('../background.png')
      }
    }];
    return (

      <Image
        source={{ uri: props.currentMessage.image }}
        style={styles.image}
      />

    );
  }
  renderActions(props) {
    return (
      <Actions
        {...props}

        icon={() => (
          <Icon
            name={"camera"}
            size={30}
            color={'red'}
            font={"FontAwesome"}
            onPress={this.selectFile}
          />
        )}
        onSend={(args) => console.log(args)}
      />
    );
  }
  // Message Time
  // messageTime = (props) => {
  //   //  console.log(props.currentMessage.createdAt)
  //   return (
  //     <View style={props.containerStyle}>
  //       <Text style={{ marginHorizontal: 10, marginBottom: 5, color: props.position === "left" ? 'black' : 'white' }} >
  //         {props.currentMessage.createdAt} </Text>
  //     </View>
  //   )
  // }
  onSend(messages = []) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });
  }
  render() {
    const rightButtonConfig = {
      title: 'Add photo',
      handler: () => this.selectFile(),
    };
    return (
      <>
        <View style={{ flex: 1 }}>
          <NavigationBar
            title={{ title: "chat" }}
            rightButton={rightButtonConfig}
          />
          {this.state.messages.length === 0 && (
            <View style={[
              StyleSheet.absoluteFill,
              {
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
                bottom: 50
              }]}>
              <Image
                source={{ uri: 'https://i.stack.imgur.com/qLdPt.png' }}
                style={{
                  ...StyleSheet.absoluteFillObject,
                  resizeMode: 'contain'
                }}
              />
            </View>
          )}
          <GiftedChat
            messages={this.state.messages}
            onSend={this.onSend}
            alwaysShowSend={true}
            renderInputToolbar={this.customInputToolbar}
            // renderMessage={this.renderMessage}
            renderMessageImage={this.renderMessageImage}
            bottomOffset={0}
            //        renderActions={this.renderActions}
            //  renderActions={() => {
            //     return (
            //       <TouchableOpacity  style={{flex:0.2}} onPress={this.selectFile}>
            //       <Ions
            //         name="ios-mic"
            //         size={35}
            //         color={this.state.startAudio ? "red" : "black"}
            //         style={{
            //          // bottom: 50,
            //           //right: Dimensions.get("window").width / 2,
            //           position: "absolute", 
            //           shadowColor: "#000",
            //           shadowOffset: { width: 0, height: 0 },
            //           shadowOpacity: 0.5,
            //           zIndex: 2,
            //           backgroundColor: "transparent"
            //         }}
            //        // 

            //       />
            //        </TouchableOpacity>);

            //   }}
            //  renderSend={this.renderSend}
            // renderTime={this.messageTime}
            user={{
              _id: 1,
            }}
          />
        </View>
      </>
    );
  }
}

export default ChatScreen