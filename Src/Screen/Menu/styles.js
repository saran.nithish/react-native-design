import { StyleSheet } from 'react-native';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
////font and color
import { fontProperties } from '../../globlaStyle/font';
import globalColor from '../../globlaStyle/color'

export default StyleSheet.create({
    backgroundColor: {
        backgroundColor: globalColor.white
    },
    menuview:{
        flex:1,alignItems:'center',justifyContent:'center'
    }
   
});
