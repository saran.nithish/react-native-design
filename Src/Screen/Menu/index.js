import React from 'react';
import { StatusBar } from 'react-native';
import { View, Text, Image } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globlaStyle/styles';
import styles from './styles';
import globalColor from '../../globlaStyle/color'
/////library
import Font from 'react-native-vector-icons/FontAwesome'
import Ions from 'react-native-vector-icons/Ionicons'
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DropShadow from "react-native-drop-shadow";
///components
import ProductList from '../../Components/Productlist';
import {imagepath} from '../../globlaStyle/imagePaths';


const MenuScreen = (props) => {
    

    return (

        <View style={[globlaStyle.global,styles.backgroundColor]}>
            <StatusBar backgroundColor={globalColor.primaryColor} />
            <View style={styles.menuview}>
                <Text>menu</Text>
            </View>
               
        </View>
    )
};



export default MenuScreen;
