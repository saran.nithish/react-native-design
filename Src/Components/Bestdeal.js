import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { View, Text, TouchableOpacity, FlatList, TouchableHighlight, Image, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Entypo from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/Ionicons';
import globlaStyle from '../globlaStyle/styles';
import globalColor from '../globlaStyle/styles';
import { fontProperties } from '../globlaStyle/font';
import Ions from 'react-native-vector-icons/Ionicons'
import Stars from 'react-native-stars';
import Mat from 'react-native-vector-icons/MaterialCommunityIcons';

const Bestdeal = (props) => {
    const [skills, setskills] = React.useState([
        {
            id: '1',
            title: 'Job title',


        },
        {
            id: '2',
            title: 'Request Date: ',

        },

        {
            id: '3',
            title: 'Name',

        },
        {
            id: '4',
            title: 'Name',

        },
    ])

    return (

        <View style={[globlaStyle.global, styles.backgroundColor]}>
            <StatusBar translucent backgroundColor="transparent" />
            <ScrollView>
                <View style={styles.subCont}>
                    <FlatList
                        data={props.data}
                        keyExtractor={item => item.id}
                        renderItem={({ item, index }) => {
                            return (
                                <View style={styles.boxstyle}>
                                    <View style={styles.innermain}>
                                        <View style={styles.innersub}>
                                            <View style={globlaStyle.row}>
                                                <View style={styles.cartimgview}>
                                                    <Image
                                                        source={item.image}
                                                        style={styles.cartimage} />

                                                </View>
                                                <View style={styles.cartproductview}>
                                                    <View style={styles.productline}>
                                                        <View style={styles.cartproducttitleview}>
                                                            <Text style={styles.carttitle}>Grand Royal Hotal</Text>
                                                            <Text style={styles.cartcolortext}>Bracelona,Spain</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row',paddingTop:hp('4%') }}>
                                                            <View style={{ flex: 1 }}>
                                                                <View style={{ flexDirection: 'row', }}>
                                                                    <Ions
                                                                        name="md-location-sharp"
                                                                        size={hp('2.5%')}
                                                                        color={'#54D3C2'}
                                                                        style={styles.carticonstyle}
                                                                    />
                                                                    <Text style={styles.km}>
                                                                        2 km to only
                                                                    </Text>
                                                                </View>
                                                                <Stars
                                                                    default={3}
                                                                    count={5}
                                                                    //half={true}
                                                                    // starSize={10000}
                                                                    fullStar={<Mat name={'star'} size={18} color={'#54D3C2'} style={[styles.myStarStyle]} />}
                                                                    emptyStar={<Mat name={'star-outline'} size={18} color={'#54D3C2'} style={[styles.myStarStyle, styles.myEmptyStarStyle]} />}

                                                                />
                                                            </View>
                                                            <View style={{ flex: 1, alignItems: 'flex-end', right: hp('2%') }}>
                                                                <Text style={styles.price}>
                                                                    $180
                                                                </Text>
                                                                <Text style={styles.day}>
                                                                  /per night
                                                                </Text>
                                                            </View>
                                                        </View>


                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            )
                        }}
                    />
                </View>
            </ScrollView>
        </View>
    )
};



export default Bestdeal;

const styles = StyleSheet.create({
    /////////cartstyle////////
    subCont: {
        flex: 1,
        paddingVertical: hp('1%')
        // margin: hp('2%')
    },
    carttitleview: {
        paddingVertical: hp('1.5%')
    },
    boxstyle: {
        flex: 1,
        paddingBottom: hp('1.8%'),
        paddingHorizontal: hp('2%'),
        right: hp('.5%')
    },
    cartimage: {
        width: hp('18%'),
        height: hp('18%'),
        borderTopLeftRadius: hp('1.8%'),
        borderBottomLeftRadius: hp('1.8%')
        //borderRadius: hp('2%')
    },
    cartimgview: {
        flex: 1
    },
    cartpricetext: {
        // fontFamily: fontProperties.semiBold,
        fontSize: hp('1.8%')
    },
    cartpriceView: {
        marginLeft: hp('1%'),
        top: hp('1.5%')
    },
    addno: {
        // fontFamily: fontProperties.semiBold,
        top: hp('.2%')
    },
    carttitle: {
        fontSize: hp('2.2%'),
        fontFamily: fontProperties.Medium
    },
    cartcolortext: {
        color: '#333',
        fontSize: hp('1.4%'),
         fontFamily: fontProperties.bold, 
        bottom: hp('.3%')
    },
    cartproductview: {
        flex: 1.3,
        // right: hp('4%'),
    },
    cartproducttitleview: {
        paddingVertical: hp('.5%'),
        // marginLeft: hp('.5%')
    },
    checkBtnView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    checkbtntext: {
        /// fontFamily: fontProperties.semiBold,
        fontSize: hp('2.5%'),
        textAlign: 'center'
    },
    totalpricetext: {
        // fontFamily: fontProperties.semiBold,
        fontSize: hp('3%')
    },
    totaltitle: {
        color: '#B2B2B2',
        //fontFamily: fontProperties.Medium,
        fontSize: hp('2%')
    },
    totalpriceview: {
        bottom: hp('1%')
    },
    productline: {
        // borderBottomWidth: hp('.15%'),
        //borderBottomColor: '#CFCFCF',
        paddingVertical: hp('1%'),
       
    },
    foot: {
        flexDirection: 'row',
        top: hp('2%')
    },
    footer: {
        borderTopWidth: hp('.15%'),
        borderTopColor: '#CFCFCF',
        margin: hp('2%'),
        bottom: hp('1%')
    },
    addbtntext: {
        justifyContent: 'center',
        marginLeft: hp('.5%')
    },
    addbtnview: {
        backgroundColor: globalColor.primaryColor,
        width: hp('3%'),
        alignItems: 'center',
        borderRadius: hp('1%')
    },
    mineiconview: {
        marginLeft: hp('.5%')
    },
    tabstyles: {
        backgroundColor: '#fff',
        borderTopWidth: 2,
        borderTopColor: '#E7E7E7'
    },
    innermain: {
        backgroundColor: '#F9F9F9',
        borderRadius: hp('2%'),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    price:{
     fontSize:hp('2.2%'),
     fontFamily: fontProperties.semiBold,
    },
    day:{
        fontSize:hp('1.2%'),
        fontFamily: fontProperties.regular,
        color:'#333'
    },
    km:{
        fontSize:hp('1.9%'),
        fontFamily: fontProperties.regular,
        color: '#C2C2C2'
    }
});
