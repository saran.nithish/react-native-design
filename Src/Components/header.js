import React, { Component } from 'react';
import { View, Text, Image, Platform, Keyboard, StyleSheet, Alert, TouchableOpacity } from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Header, ThemeConsumer } from 'react-native-elements';
import globalColor from '../globlaStyle/color';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';


class HeaderComponent extends Component {
  constructor(props) {
    super(props);
    this.navigationParams =
      this.props && this.props.route && this.props.route.params;

    this.state = {

    };
  }

  deletepost() {
    this.postdeleteService()

  }



  leftHandler = (navigation) => {
    if (this.props.left === 'menu') {
      return (
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.toggleDrawer();
            Keyboard.dismiss();

          }}>
          <Icon
            name="ios-menu"
            size={25}
            color="#0D2D28"
           // backgroundColor="#44506A"
          />
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  };


  render(props) {

    return (
      <Header
        placement="left"
        containerStyle={styles.header}
        leftComponent={this.leftHandler()}
        centerComponent={{ text: this.props.title, style: styles.title }}
       

      />
    );
  }
}

// const HeaderComponent = () =>{

//   return (
//           <Header
//             placement="left"
//             containerStyle={styles.header}
//             leftComponent={this.leftHandler()}
//             centerComponent={{ text: this.props.title, style: styles.title }}
//             rightComponent={this.myRightComponent()}
    
//           />
//   )

// }

const styles = StyleSheet.create({
  header: {
    backgroundColor:globalColor.primaryColor,
  },
  pageHeader: {
    flex: 1,
  },
  headerWrap: {
    width: wp('100%'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor:globalColor.primaryColor,
    paddingTop: Platform.OS === 'android' ? null : hp('3%'),
    height: Platform.OS === 'android' ? hp('7.4%') : hp('9.4%'),
  },
  // left
  leftWrap: {
    width: wp('20%'),
    alignItems: 'flex-start',
    // backgroundColor: 'red',
  },
  leftContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backIcon: {
    fontSize: hp('4%'),
    color: '#fff',
  },
  leftLink: {
    fontSize: hp('2%'),
    color: '#fff',
  },
  // middle
  midWrap: {
    width: wp('60%'),
    alignItems: 'center',

    // backgroundColor: 'green',
  },
  title: {
    top:3,
    color: '#040000',
    fontSize: hp('2%'),
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  
 


});

export default HeaderComponent;
