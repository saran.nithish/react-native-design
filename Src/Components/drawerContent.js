import React, { useState } from 'react';
import {  StyleSheet, View, } from 'react-native';
// Libraries
import Ion from 'react-native-vector-icons/Ionicons';
import { DrawerItem, DrawerContentScrollView } from '@react-navigation/drawer';
import { Drawer,} from 'react-native-paper';
// Style
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const DrawerContent = (props) => {

    return (
        <View style={{ flex: 1 }}>
            <View style={styles.drawerContent}>
                <DrawerContentScrollView
                    {...props}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}>
                        <Drawer.Section style={styles.drawerSection}>
                            <DrawerItem
                                icon={({ color, size }) => (
                                    <Ion name="home-outline" color={color} size={size} />
                                )}
                                label="Home"
                                onPress={() => {
                                    props.navigation.navigate('Home')
                                }}
                            />
                             <DrawerItem
                                icon={({ color, size }) => (
                                    <Ion name="home-outline" color={color} size={size} />
                                )}
                                label="Home"
                                onPress={() => {
                                    props.navigation.navigate('Homes')
                                }}
                            />
                              <DrawerItem
                                icon={({ color, size }) => (
                                    <Ion name="home-outline" color={color} size={size} />
                                )}
                                label="Home"
                                onPress={() => {
                                    props.navigation.navigate('productlist')
                                }}
                            />
                        </Drawer.Section>
                        

                </DrawerContentScrollView>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    drawerContent: {
        width: '100%',
        height: '100%',
    },
  
   
    drawerSection: {
        //bottom: hp('5%')
    },
  

});


export default DrawerContent;
