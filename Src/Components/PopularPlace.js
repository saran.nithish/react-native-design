import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
///Styles
import globlaStyle from '../globlaStyle/styles';
//import styles from './styles';
///color
import globalColor from '../globlaStyle/color'
////library
import Font from 'react-native-vector-icons/FontAwesome'
import Ions from 'react-native-vector-icons/Ionicons'
import { fontProperties } from '../globlaStyle/font';
import { ScrollView } from 'react-native-gesture-handler';

const popular = (props) => {

    const homelist = props.data

    return (
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {homelist.map(item => {
                return (
                    <TouchableOpacity>

                        <View key={item.id} style={styles.productimageView}>
                            <ImageBackground
                                imageStyle={{ borderRadius: hp('2%') }}
                                source={item.image}
                                style={styles.productimage} >
                                <View style={styles.textView}>
                                <Text style={styles.text}>
                                    Spain
                                </Text>
                                </View>
                                 </ImageBackground>
                        </View>
                    </TouchableOpacity>
    )
})
            }
        </ScrollView >
    )
};


export default popular;

const styles = StyleSheet.create({
    productimage: {
        width: hp('25%'),
        height: hp('15%'),
        borderRadius: hp('2%')
    },
    productimageView: {
        flex: 1,
        //right: hp('1.5%'),
        paddingHorizontal: hp('.5%'),
        //borderBottomColor: globalColor.bottomColor,
        //borderBottomWidth: hp('.15%'),
        paddingVertical: hp('1%')
    },
    text: {
        fontSize: hp('3%'),
        color: '#fff',
        fontFamily:fontProperties.bold

    },
    textView:{
        flex:1,alignItems:'flex-end',justifyContent:'flex-end',right:hp('2%')
    }
});

