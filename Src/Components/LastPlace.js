import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
///Styles
import globlaStyle from '../globlaStyle/styles';
//import styles from './styles';
///color
import globalColor from '../globlaStyle/color'
////library
import Font from 'react-native-vector-icons/FontAwesome'
import Ions from 'react-native-vector-icons/Ionicons'
import { fontProperties } from '../globlaStyle/font';
import { ScrollView } from 'react-native-gesture-handler';

const LastPlace = (props) => {

    const homelist = props.data

    return (
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {homelist.map(item => {
                return (
                    <TouchableOpacity>
                        <View  Key={item.id} style={{ paddingHorizontal: hp('1%'),paddingVertical:hp('1%') }}>
                            <View style={styles.mainCont}>
                                <View key={item.id} style={styles.productimageView}>
                                    <Image

                                        source={item.image}
                                        style={styles.productimage} />
                                </View>
                                <View style={styles.mainText}>
                                <View>
                                    <Text style={styles.title}>
                                        London
                                    </Text>
                                </View>
                                <View>
                                    <Text style={styles.roomText}>
                                        1 Room  - 2 Adults
                                    </Text>
                                </View>
                                <View>
                                    <Text style={styles.roomText}>
                                        12 - 22 Dec
                                    </Text>
                                </View>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                )
            })
            }
        </ScrollView>
    )
};


export default LastPlace;

const styles = StyleSheet.create({
    productimage: {
        width: hp('15%'),
        height: hp('15%'),
        borderTopRightRadius:hp('1.8%'),
        borderTopLeftRadius:hp('1.8%'),
        //borderRadius: hp('2%')
    },
    productimageView: {
        // flex: 1,
        width: '100%',
        //right: hp('1.5%'),

        //borderBottomColor: globalColor.bottomColor,
        //borderBottomWidth: hp('.15%'),
        //paddingVertical: hp('1%')
    },
    roomText: {
        fontSize: hp('1.2%'),
        fontFamily:fontProperties.regular,
        color: '#C2C2C2'
    },
    mainCont: {
        backgroundColor: '#fff',
        borderRadius: hp('2%'),
        //paddingVertical:hp('1%'),
        borderColor:'#C2C2C2',
        borderWidth: hp('.15%'),
        flex: 1
    },
    mainText:{
        paddingVertical:hp('1%'),
        paddingHorizontal:hp('2%')
    },
    title:{
        fontSize: hp('2%'),
        fontFamily:fontProperties.semiBold
        //color: '#C2C2C2'
    }
});

