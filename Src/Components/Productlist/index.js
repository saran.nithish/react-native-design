import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
///Styles
import globlaStyle from '../../globlaStyle/styles';
import styles from './styles';
///color
import globalColor from '../../globlaStyle/color'
////library
import Font from 'react-native-vector-icons/FontAwesome'
import Ions from 'react-native-vector-icons/Ionicons'


const HomeScreen = (props) => {
  
    const homelist =  props.data
 
    return (
        <View>
            {homelist.map(item => {
                return (
                    <TouchableOpacity onPress={() => { props.navigation.navigate('TableDetails') }}>
                        <View style={globlaStyle.row}>
                            <View style={styles.hotproductview}>
                                <View>
                                    <Text style={styles.producttitle}>Dinner Set</Text>
                                </View>
                                <View style={styles.addressView}>
                                    <Text style={styles.addressText} >Dinner Set Adaadada</Text>
                                </View>
                                <View>
                                    <Text style={styles.priceText} >{'Rp 450000'}</Text>
                                </View>
                                <View style={globlaStyle.row}>
                                    <Font
                                        name="star"
                                        size={hp('2%')}
                                        color={styles.starcolor}
                                        style={styles.iconstyle}
                                    />
                                    <View style={styles.starviewText}>
                                        <Text style={styles.rateText}>4.5</Text>
                                    </View>
                                </View>
                                <View style={styles.heartView}>
                                    <Font
                                        name="heart"
                                        size={hp('3%')}
                                        color={globalColor.heartcolor}
                                        style={styles.iconstyle}
                                    />
                                    <View style={styles.cartstyle}>
                                        <View style={styles.circleShap}>
                                            <Ions
                                                name="cart-outline"
                                                size={hp('2.5%')}
                                                color={globalColor.cartcolor}
                                                style={styles.carticonstyle}
                                            />
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.productimageView}>
                                <Image
                                    source={item.image}
                                    style={styles.productimage} />
                            </View>
                        </View>
                    </TouchableOpacity>
                )
            })
            }
        </View>
    )
};

export default HomeScreen;