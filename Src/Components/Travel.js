import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
///Styles
import globlaStyle from '../globlaStyle/styles';
//import styles from './styles';
///color
import globalColor from '../globlaStyle/color'
////library
import Font from 'react-native-vector-icons/FontAwesome'
import Ions from 'react-native-vector-icons/Ionicons'
import { fontProperties } from '../globlaStyle/font';
import { ScrollView } from 'react-native-gesture-handler';

const Travel = (props) => {

    const homelist = props.data

    return (
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {homelist.map(item => {
                return (
                    <TouchableOpacity key={item.id} onPress={() => { props.navigation.navigate('PlaceDetails') }}>

                        <View style={styles.productimageView}>
                            <View style={styles.circle}>
                                <Image
                                    source={item.icon}
                                    resizeMode='contain'
                                    style={styles.productimage}
                                />
                            </View>
                            <Text style={styles.titleText}>
                                {item.title}
                            </Text>
                        </View>
                    </TouchableOpacity>
                )
            })
            }
        </ScrollView>
    )
};


export default Travel;

const styles = StyleSheet.create({
    productimage: {
        width: hp('8%'),
        height: hp('5%'),
        //borderRadius: hp('2%')
    },
    productimageView: {
        //  flex: 1,
        //right: hp('1.5%'),
        paddingHorizontal: hp('1.2%'),
        //borderBottomColor: globalColor.bottomColor,
        //borderBottomWidth: hp('.15%'),
        paddingVertical: hp('2%')
    },
    text: {
        fontSize: hp('3%'),
        color: '#fff'

    },
    textView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        right: hp('2%')
    },
    circle: {
        width: hp('10%'),
        height: hp('10%'),
        borderRadius: hp('15%'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#E7E7E7',
    },
    titleText: {
        textAlign: 'center', top: hp('2%'), fontSize: hp('2%'),
        fontFamily:fontProperties.regular
    }
});

