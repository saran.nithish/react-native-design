export const fontProperties = {
    bold: 'Montserrat-Bold',
    italic: 'Poppins-Italic',
    italicbold: 'Poppins-SemiBoldItalic',
    regular: 'Poppins-Regular',
    semiBold: 'Montserrat-SemiBold',
    extraBold: 'Poppins-ExtraBold',
    Medium:'Montserrat-Medium'
  };
