import { StyleSheet } from 'react-native';
///font & color
import { fontProperties } from '../../globlaStyle/font';
import globalColor from '../../globlaStyle/color';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';

export default StyleSheet.create({
 global:{
     width:'100%',
     height:'100%',
 },
 row:{
     flexDirection:'row',
 },
 flex:{
     flex:1
 },
 flex2:{
    flex:1.5
},
 title: {
    fontSize: hp('2.5%'),
    fontFamily: fontProperties.semiBold,
    color: globalColor.black
},
subtitle: {
    fontSize: hp('2%'),
    color:globalColor.black,
    fontFamily:fontProperties.semiBold
},
profileimage: {
    width: hp('7%'),
    height: hp('7%'),
    borderRadius: hp('7%')
},
dectext:{
    fontSize: hp('1.5%'),
    fontFamily: fontProperties.regular,
    color: globalColor.black
},
colorcricleview:{
    bottom:hp('1%')
}

  
});
