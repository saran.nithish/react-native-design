import { DarkTheme as NavigationDarkTheme} from '@react-navigation/native';
import { DarkTheme as PaperDarkTheme} from 'react-native-paper';

export default CustomDarkTheme = {
    ...NavigationDarkTheme,
    ...PaperDarkTheme,
    colors: {
        ...NavigationDarkTheme.colors,
        ...PaperDarkTheme.colors,
        text: "#fff",
        background: "#1E2125",
        background2: '#1F2226',
        contained: '#000000',
        buynow: '#fff',
        addbt: '#16181A',
        menuicon: '#fff',
        buytext: '#333',
    }
}