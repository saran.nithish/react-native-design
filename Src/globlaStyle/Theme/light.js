import { DefaultTheme as NavigationDefaultTheme  } from '@react-navigation/native';
import { DefaultTheme as PaperDefaultTheme,} from 'react-native-paper';

  export default  CustomDefaultTheme  = {
    ...PaperDefaultTheme,
    background: '#fff',
    roundness: 8,
    colors: {
      ...NavigationDefaultTheme.colors,
      ...PaperDefaultTheme.colors,
      text: "#333",
      background: "#E6EEF6",
      background2: '#EEF3F8',
      contained: '#000000',
      buynow: '#1B1A21',
      buytext: '#fff',
      addbt: '#A1B8CF',
      menuicon: '#333'
    },
  
  };