export const colors = {
  ///primarycolor
    white: '#fff',
    black: '#000',
    inputicons: '#3c3c3c',
    primaryColor: '#FFCD1C',
    bottomColor: '#D3D3D3',
  ///iconcolor
     heartcolor:'#E3278D',
     cartcolor:'#000',
     starcolor:'#FFC802',
     downarrowcolor:'#D7D7D7',
     chatcolor:'#757575',
     bellcolor:'##4D3E08',
     ////foodappIcon
     Iconcolor:'#fff'
    
  };
  
  export default colors;
