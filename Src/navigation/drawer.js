import React from 'react';
// Libraries
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
///component
import HomeScreen from '../Screen/HomeScreen';
import MenuScreen from '../Screen/Menu';
import DrawerContent from '../Components/drawerContent';
import Header from '../Components/header';

const Drawer = createDrawerNavigator();
const HomeStack = createStackNavigator();

const HomeStackScreen = ({ navigation, navigation: { goBack } }) => (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Home"
        component={MenuScreen}
        options={{
          header: (props) => (
            <Header
              left="menu"
              navigation={props.navigation}
            />
          ),
        }}
      />
      <HomeStack.Screen
        name="Homes"
        component={HomeScreen}
        // options={{
        //   header: (props) => (
        //     <Header
        //       left="menu"
        //       navigation={props.navigation}
        //     />
        //   ),
        // }}
      />
  
    </HomeStack.Navigator>
  );
  

export const DrawerStack = () => {
    return (
      <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
        <Drawer.Screen
          name="Home"
          component={HomeStackScreen}
          options={{
            headerShown: false,
          }}
        />
        <Drawer.Screen
          name="Homes"
          component={HomeScreen}
          options={{
            headerShown: false,
          }}
        />
  
      </Drawer.Navigator>
    );
  };
  