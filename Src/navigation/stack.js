import React from 'react';
// Library
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../Screen/HomeScreen';
import TableDetails from '../Screen/TableDetails';
import Foodapp from '../Screen/Foodapp';
import MenuScreen from '../Screen/Menu';
import Fashion from '../Screen/Fashion'
import { DrawerStack } from './drawer'
import HotalBooking from '../Screen/HotalBooking';
import Japan from '../Screen/japan';
import PlaceDetails from '../Screen/PlaceDetails';
import GoogleLogin from '../Screen/GoogleLogin';
import chat from '../Screen/chat';




const Stack = createStackNavigator();

const StackScreens = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="Chat" headerMode="screen">

      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerShown: false
        }}
      />
      <Stack.Screen
        name="TableDetails"
        component={TableDetails}
        options={{
          headerShown: false
        }}
      />
      <Stack.Screen
        name="FoodApp"
        component={Foodapp}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen
        name="Menu"
        component={MenuScreen}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen
        name="Fashion"
        component={Fashion}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen
        name="Hotal"
        component={HotalBooking}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen
        name="Japan"
        component={Japan}
        options={{
          headerShown: false
        }}
      />


      <Stack.Screen
        name="PlaceDetails"
        component={PlaceDetails}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen
        name="Login"
        component={GoogleLogin}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen
        name="Chat"
        component={chat}
        options={{
          headerShown: false
        }}
      />
    </Stack.Navigator>
  );
};

export default StackScreens;