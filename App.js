import React, { useEffect } from 'react';
///navigationlibray
import { NavigationContainer, } from '@react-navigation/native';
import StackScreens from './Src/navigation/stack';
////library
import { Provider as PaperProvider, } from 'react-native-paper';
import { EventRegister } from 'react-native-event-listeners';
////themes
import lightTheme from './Src/globlaStyle/Theme/light';
import DarkTheme from './Src/globlaStyle/Theme/Dark';


const App = () => {

  const [isDarkTheme, setIsDarkTheme] = React.useState(false);
  const theme = isDarkTheme ? DarkTheme : lightTheme;

  useEffect(() => {
    let evenListener = EventRegister.addEventListener('changeThemeEvent', (data) => {
      setIsDarkTheme(data)
    })
    return () => {
      EventRegister.removeEventListener(evenListener)
    }
  }, [])

  return (

    <PaperProvider theme={theme}>
      <NavigationContainer theme={theme}>
        <StackScreens />
      </NavigationContainer>
    </PaperProvider>

  );
}

export default App;